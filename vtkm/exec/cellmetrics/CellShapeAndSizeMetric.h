//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  Copyright 2018 National Technology & Engineering Solutions of Sandia, LLC (NTESS).
//  Copyright 2018 UT-Battelle, LLC.
//  Copyright 2018 Los Alamos National Security.
//
//  Under the terms of Contract DE-NA0003525 with NTESS,
//  the U.S. Government retains certain rights in this software.
//
//  Under the terms of Contract DE-AC52-06NA25396 with Los Alamos National
//  Laboratory (LANL), the U.S. Government retains certain rights in
//  this software.
//============================================================================
#ifndef vtk_m_exec_cellmetrics_CellShapeAndSizeMetric_h
#define vtk_m_exec_cellmetrics_CellShapeAndSizeMetric_h

/*
 * Mesh quality metric functions that compute the shape and size of mesh cells.
 * The shape and size metric is defined by the relative size squared metric
 * times the shape metric
 *
 * This metric was designed in context of Sandia's Pronto code for stable time
 * step calculation. 
 *
 * These metric computations are adapted from the VTK implementation of the
 * Verdict library, which provides a set of mesh/cell metrics for evaluating the
 * geometric qualities of regions of mesh spaces.
 *
 * See: The Verdict Library Reference Manual (for per-cell-type metric formulae)
 * See: vtk/ThirdParty/verdict/vtkverdict (for VTK code implementation of this
 * metric)
 */

#include "vtkm/CellShape.h"
#include "vtkm/CellTraits.h"
#include "vtkm/VecTraits.h"
#include "vtkm/VectorAnalysis.h"
#include "vtkm/exec/FunctorBase.h"

#define UNUSED(expr) (void)(expr);

namespace vtkm
{
namespace exec
{
namespace cellmetrics
{

using FloatType = vtkm::FloatDefault;

// ========================= Unsupported cells ==================================

// ShapeAndSize is only defined for Hexahedral cell typesk
template <typename OutType, typename PointCoordVecType, typename CellShapeType>
VTKM_EXEC OutType CellShapeAndSizeMetric(const vtkm::IdComponent& numPts,
                                      const PointCoordVecType& pts,
                                      CellShapeType shape,
                                      const vtkm::exec::FunctorBase&)
{
  UNUSED(numPts);
  UNUSED(pts);
  UNUSED(shape);
  return OutType(-1.);
}

template <typename OutType, typename PointCoordVecType>
VTKM_EXEC OutType CellShapeAndSizeMetric(const vtkm::IdComponent& numPts,
                                      const PointCoordVecType& pts,
                                      const OutType& rss,
                                      const OutType& shape,
                                      vtkm::CellShapeTagTriangle,
                                      const vtkm::exec::FunctorBase& worklet)
{
    UNUSED(numPts);
    UNUSED(pts);
    UNUSED(worklet);
    return OutType(rss*shape);
}

template <typename OutType, typename PointCoordVecType>
VTKM_EXEC OutType CellShapeAndSizeMetric(const vtkm::IdComponent& numPts,
                                      const PointCoordVecType& pts,
                                      const OutType& rss,
                                      const OutType& shape,
                                      vtkm::CellShapeTagQuadrilateral,
                                      const vtkm::exec::FunctorBase& worklet)
{
    UNUSED(numPts);
    UNUSED(pts);
    UNUSED(worklet);
    return OutType(rss*shape);
}

// ========================= 3D cells ==================================
  
template <typename OutType, typename PointCoordVecType>
VTKM_EXEC OutType CellShapeAndSizeMetric(const vtkm::IdComponent& numPts,
                                      const PointCoordVecType& pts,
                                      const OutType& rss,
                                      const OutType& shape,
                                      vtkm::CellShapeTagTetrahedral,
                                      const vtkm::exec::FunctorBase& worklet)
{
    UNUSED(numPts);
    UNUSED(pts);
    UNUSED(worklet);
    return OutType(rss*shape);
}

template <typename OutType, typename PointCoordVecType>
VTKM_EXEC OutType CellShapeAndSizeMetric(const vtkm::IdComponent& numPts,
                                      const PointCoordVecType& pts,
                                      const OutType& rss,
                                      const OutType& shape,
                                      vtkm::CellShapeTagHexahedral,
                                      const vtkm::exec::FunctorBase& worklet)
{
    UNUSED(numPts);
    UNUSED(pts);
    UNUSED(worklet);
    return OutType(rss*shape);
}

} // cell metrics
} // exec
} // vtkm

#endif // vtk_m_exec_cellmetrics_CellShapeAndSizeMetric_h
